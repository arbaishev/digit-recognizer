# -*- coding: utf-8 -*- 

import csv
import numpy as np

def vectorized_result(j):
    """Преобразует цифры 0-9 в соответствии с выходом нейронной сети и возвращает 10-и мерный единичный вектор с 1 в позиции j."""
    e = np.zeros((10, 1))
    e[j] = 1.0
    return e

def load_data(train_count = 1000, test_count = 200):
    with open('train_big.csv', 'rb') as train_fd:
        reader = csv.reader(train_fd)
        reader.next()

        count = 0

        train_input = []
        train_res = []
        test_input = []
        test_res = []

        for row in reader:
            row_to_float = [float(elem) for elem in row]

            if count < train_count:
                y = row_to_float[0]
                row_to_float.pop(0)
                x = [val/255.0 for val in row_to_float]
                y = vectorized_result(int(y))
                train_input.append(x)
                train_res.append(y)


            elif count < test_count+train_count:
                y = row_to_float[0]
                row_to_float.pop(0)
                x = [val/255.0 for val in row_to_float]
                test_input.append(x)
                test_res.append(int(y))

            else:
                break

            count = count + 1

        train_input = [np.reshape(x, (784, 1)) for x in train_input]
        train_res = np.array(train_res)

        test_input = [np.reshape(x, (784, 1)) for x in test_input]
        test_res = np.array(test_res)

        train_data = zip(train_input,train_res)
        testing_data = zip(test_input,test_res)

    return train_data, testing_data

import network

def train_network():
    train_data, test_data = load_data(5000, 1000)
    net = network.Network([784, 30, 10])
    net.SGD(train_data, 30, 10, 1.0, test_data)

    test_input = []

    with open('test_big.csv', 'rb') as f_testing:
        reader = csv.reader(f_testing)
        reader.next()
        count = 0;
        for row in reader:
            row_to_float = [float(elem) for elem in row]
            x = [val/255.0 for val in row_to_float]
            test_input.append(x)
        test_input = [np.reshape(x, (784, 1)) for x in test_input]
        
    outputs = []
    for x in test_input:
        y = np.argmax(net.feedforward(x))
        outputs.append(y)

    with open('submission.csv', 'wb') as f:
        writer = csv.writer(f)
        outputs = np.reshape(outputs,(len(outputs),1))
        writer.writerows(outputs)
        f.close()