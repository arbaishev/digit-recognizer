# -*- coding: utf-8 -*- 

import random

import numpy as np

"""Градиенты вычисляются с помошью метода обратного распространения ошибки (backpropagation)"""
class Network():

    def __init__(self, sizes):
        """Список 'sizes' содержит количество нейронов на соответствующем уровне."""
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(y, x) 
                        for x, y in zip(sizes[:-1], sizes[1:])]

    def print_all():
        print self.weights
        print self.biases

    def feedforward(self, a):
        """Возвращает выход из сети, если 'a' было входом"""
        for b, w in zip(self.biases, self.weights):
            a = sigmoid_vec(np.dot(w, a)+b)
        return a

    def SGD(self, train_data, epochs, mini_batch_size, eta,
            test_data=None):
        """Обучение нейронной сети с помощью градиентного спуска
        Параметр 'train_data' это пары значений: учебный материал и желаемый результат.
        После каждого цикла обучения идет тестирование на тестовой выборке и выводится результат"""
        if test_data: n_test = len(test_data)
        n = len(train_data)
        for j in xrange(epochs):
            random.shuffle(train_data)
            mini_batches = [train_data[k:k+mini_batch_size]
                            for k in xrange(0, n, mini_batch_size)]
            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch, eta)
            if test_data:
                print "Epoch {}: {} / {}. Errors: {}%".format(
                    j, self.evaluate(test_data), n_test, 100-self.evaluate(test_data) * 100 / n_test)
            else:
                print "Epoch %s complete" % j

    def update_mini_batch(self, mini_batch, eta):
        """Обновление весов и смещений сети с использованием градиентного спуска и метода обратного распространения ошибки"""
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        for x, y in mini_batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)
            nabla_b = [nb+dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw+dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]
        self.weights = [w-(eta/len(mini_batch))*nw 
                        for w, nw in zip(self.weights, nabla_w)]
        self.biases = [b-(eta/len(mini_batch))*nb 
                       for b, nb in zip(self.biases, nabla_b)]

    def backprop(self, x, y):
        """Возвращает пару значений - градиент функции ошибок(затрат)"""
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        activation = x
        activations = [x] #список всех активаций
        zs = [] 
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation)+b
            zs.append(z)
            activation = sigmoid_vec(z)
            activations.append(activation)
        delta = self.cost_derivative(activations[-1], y) * \
            sigmoid_prime_vec(zs[-1])
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-2].transpose())

        for l in xrange(2, self.num_layers):
            z = zs[-l]
            spv = sigmoid_prime_vec(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * spv
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-l-1].transpose())
        return (nabla_b, nabla_w)

    def evaluate(self, test_data):
        """Возвращает количество тестовых данных, для которых нейронная сеть выводит правильный результат."""
        test_results = [(np.argmax(self.feedforward(x)), y) 
                        for (x, y) in test_data]
        return sum(int(x == y) for (x, y) in test_results)
        
    def cost_derivative(self, output_activations, y):
        return (output_activations-y) 

def sigmoid(z):
    return 1.0/(1.0+np.exp(-z))

sigmoid_vec = np.vectorize(sigmoid)

def sigmoid_prime(z):
    """Производная функции сигмоида"""
    return sigmoid(z)*(1-sigmoid(z))

sigmoid_prime_vec = np.vectorize(sigmoid_prime)
