# DigitRecognizer

Нейронная сеть для распознавания рукописных цифр  
Тестируется и обучается на базе данных MNIST

### Необходимо
- Python 2.7
- NumPy:
    `pip install numpy`


### Как запустить

1. Запустить `run.py` в терминале.  
        `python run.py`